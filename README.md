# CLI-Calc AKA my awesome project

CLI-calculator written in Python

Project Brief from Robert Clark 6/4/2020:

·        Objective

o   Write a Python CLI tool which allows users to enter a number, operator, and another number and calculates the result and prints it out

·        Timeline

o   Half a day to 3 days depending on schedule/comfort

·        Example CLI

o   $ python calculator.py

Enter first number: 3

Enter operator: *

Enter second number: 4

Result: 12

·        Example CLI (more advanced)

o   $ python calculator.py

Enter equation: 3 * 4

Result: 12

·        Key Topics/Skills

o   Functions

·        Ideally, the input parsing should be in functions to reduce redundancy

·        Calling functions with arguments, returning results to calling functions

o   Input parsing

·        Parse input from the user via the terminal

·        Store the input and use it later

·        Validate input

§  Numbers must be int or float

§  Operators must be (+ - / *)

o   Variable extrapolation

·        Convert variables, such as the operators, into actions

o   Branching logic

·        Branch based on the requested operator

o   Error handling

·        Code should be able to handle the following without errors (but messages to the user without crashing/stack traces are fine):

§  $python calculator.py

Enter first number: 'a'