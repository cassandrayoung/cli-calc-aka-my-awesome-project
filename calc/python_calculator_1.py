# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 11:17:38 2020

@author: cassandray
"""

print('Preparing calculator ...')

num1 = int(input('Enter first number: '))

operation = input('''
Please type desired mathmatical operation:
+ addition
- subtraction
* multiplication
/ division
''')

num2 = int(input('Enter second number: '))

if operation == '+':
    print(float(num1 + num2))

elif operation == '-':
    print(float(num1 - num2))

elif operation == '*':
    print(float(num1 * num2))

elif operation == '/':
    print(float(num1 / num2))
    
else: print('Not a valid operator')